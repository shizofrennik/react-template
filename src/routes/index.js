import withAuthentication from '../views/hocs/withAuthentication';
import { Home, Auth } from '../views/pages';

const routes = [
	{
		path: '/',
		component: withAuthentication(Home),
		exact: true,
	},
	{
		path: '/auth',
		component: Auth,
		exact: true,
	},
];

export default routes;
