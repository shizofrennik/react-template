import * as types from './types';

export const fetchAuth = () => ({
	type: types.FETCH_AUTH
});

export const setUser = (user) => ({
	type: types.SET_USER,
	payload: user
});

export const changeAuthStatus = (bool) => ({
	type: types.IS_AUTHENTICATED,
	payload: bool
});
