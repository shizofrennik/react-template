import reducer from './reducers';
import * as actions from './actions'; //actions
import sagas from './sagas'; //sagas
import * as selectors from './selectors';

export const authActions = actions;
export const authSagas = sagas;
export const authSelectors = selectors;
export const authReducer = reducer;

// export default {
// 	authActions,
// 	authSagas,
// 	authSelectors,
// 	authReducer
// };

// export default reducer;