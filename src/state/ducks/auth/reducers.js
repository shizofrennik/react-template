import * as types from './types';
import { createReducer } from '../../utils';
// import { combineReducers } from 'redux';

/* State shape
{
	user: user
	isAuthenticated: bool
}
*/

// const userReducer = createReducer({})(
// 	{
// 		[types.FETCH_USER_COMPLETED]: (state, action) => action.payload.user
// 	}
// );

// export default combineReducers({
// 	user: userReducer
// });

export default createReducer({
	user: {},
	isAuthenticated: false
})(
	{
		[types.SET_USER]: (state, action) => ({ ...state, user: action.payload}),
		[types.IS_AUTHENTICATED]: (state, action) => ({ ...state, isAuthenticated: action.payload})
	}
);
