import * as types from './types';
import * as actions from './actions';
import {put, takeLatest} from 'redux-saga/effects';

function* authFlow(action) {
	try {
		const credentials = action.payload;
		/* eslint-disable */
    console.log('creds: ', credentials);
    /* eslint-enable */
		yield put({type: types.FETCH_AUTH_START});
		yield put(actions.setUser({name: 'John Doe'}));
		yield put(actions.changeAuthStatus(true));
		//fetching data here
		yield put({type: types.FETCH_AUTH_COMPLETED});
	} catch(error) {
		yield put({type: types.FETCH_AUTH_FAILED});
	}
}

export default [
	takeLatest(types.FETCH_AUTH, authFlow),
];