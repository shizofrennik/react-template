export const FETCH_AUTH = 'auth/FETCH_AUTH';
export const FETCH_AUTH_START = 'auth/FETCH_AUTH_START';
export const FETCH_AUTH_COMPLETED = 'auth/FETCH_AUTH_COMPLETED';
export const FETCH_AUTH_FAILED = 'auth/FETCH_AUTH_FAILED';

export const SET_USER = 'auth/SET_USER';

export const IS_AUTHENTICATED = 'auth/IS_AUTHENTICATED';