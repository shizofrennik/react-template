import reducer from './reducers';
import * as actions from './actions'; //actions

export const authActions = actions;
export const authReducer = reducer;

// export default {
// 	authActions,
// 	authSagas,
// 	authSelectors,
// 	authReducer
// };

// export default reducer;