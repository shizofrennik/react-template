import { all } from 'redux-saga/effects';
import { authSagas, authReducer } from './auth';

export function* rootSaga() {
	yield all([
		...authSagas,
	]);
}

export const reducers = {
	auth: authReducer,
};
