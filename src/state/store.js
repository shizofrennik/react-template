import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
// import thunkMiddleware from 'redux-thunk';
import { reducers, rootSaga } from './ducks';

export default function configureStore( initialState ) {
	const rootReducer = combineReducers( reducers );
	/* eslint-disable */
  const composeEnhancers = (process.env.NODE_ENV === 'development' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;
  /* eslint-enable */
	const sagaMiddleware = createSagaMiddleware();
	const store = createStore(
		rootReducer,
		initialState,
		composeEnhancers(
			applyMiddleware(
				sagaMiddleware
			)
		)
	);

	sagaMiddleware.run(rootSaga);
	return store;
}