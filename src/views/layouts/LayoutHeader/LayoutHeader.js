
import React from 'react';
import { Link } from 'react-router-dom';
import styles from './LayoutHeader.module.css';

const Layout = ( ) => (
	<header className={styles.container}>
		<Link to="/" className={styles.link}>Dashboard</Link>
		<Link to="/auth">Auth</Link>
	</header>
);

export default Layout;
