import React from 'react';
import styles from './HomeView.module.css';

const HomeView = ( ) => (
	<div className={styles.container}> HOME PAGE! </div>
);

export default HomeView;
